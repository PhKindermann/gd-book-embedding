package uni.wue.boem.aktionen;

import java.util.LinkedList;

import javax.swing.JButton;

/**
 * Klasse zum Verwalten von Aktionen, die rueckgaengig gemacht oder
 * wiederhergestellt werden koennen.
 * 
 * @author Dennis van der Wals
 * @version 1.0.0
 *
 */
public class AktionManager {
	private LinkedList<Aktion> aktionenRueckgaengig;
	private LinkedList<Aktion> aktionenWiederherstellen;
	private Aktion aktiveAktion;

	private JButton undo, redo;

	/**
	 * Standardkonstruktor. Initialisiert die Listen
	 */
	public AktionManager(JButton undo, JButton redo) {
		aktionenRueckgaengig = new LinkedList<Aktion>();
		aktionenWiederherstellen = new LinkedList<Aktion>();

		this.undo = undo;
		this.redo = redo;

		this.undo.setEnabled(false);
		this.redo.setEnabled(false);
	}

	/**
	 * Fuegt eine neue Aktion der Liste rueckgaengigmachbarer Aktionen hinzu.
	 * Loechst dabei die Wiederherstellungsliste.
	 * 
	 * @param neu
	 *            neue Aktion
	 */
	private void neueAktion(Aktion neu) {
		aktionenRueckgaengig.push(neu);
		aktionenWiederherstellen.clear();

		undo.setEnabled(true);
		redo.setEnabled(false);
	}

	/**
	 * Nimmt eine neue Aktion auf, speichert sie als aktiveAktion und startet
	 * sie. Beim starten, wird der relevante Ist-Zustand erfasst, sodass die
	 * Aktion rueckgaengig gemacht werden kann.
	 * 
	 * @param neu
	 *            Neue Aktion
	 */
	public void starteNeueAktion(Aktion neu) {
		if (aktiveAktion != null)
			System.err
					.println("A new action was started before completing a previous one.");

		aktiveAktion = neu;
		aktiveAktion.starten();
	}

	/**
	 * Beendet die letzte, aktive Aktion. Dabei wird der neue Ist-Zustand nach
	 * der Aktion erfasst, sodass nach dem rueckgaengig machen, die Aktion
	 * wiederhergestellt werden kann. Dabei wird die Aktion nur als rueckgaengig
	 * machbar erfasst, wenn sich die Zustaende unterscheiden.
	 */
	public void beendeAktiveAktion() {
		if (aktiveAktion == null)
			return;

		aktiveAktion.beenden();

		if (aktiveAktion.gibtEsUnterschiede())
			neueAktion(aktiveAktion);

		aktiveAktion = null;
	}

	/**
	 * Gibt zurueck, ob es rueckgaengig machbare Aktionen gibt.
	 * 
	 * @return Wahrheitswert
	 */
	public boolean kannRueckgaengig() {
		return !aktionenRueckgaengig.isEmpty();
	}

	/**
	 * Gibt zurueck, ob es wiederherstellbare Aktionen gibt.
	 * 
	 * @return Wahrheitswert
	 */
	public boolean kannWiederherstellen() {
		return !aktionenWiederherstellen.isEmpty();
	}

	/**
	 * Macht die letzte Aktion rueckgaengig und macht sie wiederherstellbar.
	 */
	public void rueckgaengig() {
		if (!kannRueckgaengig())
			return;

		Aktion a = aktionenRueckgaengig.poll();
		a.rueckgaengig();
		undo.setEnabled(kannRueckgaengig());

		aktionenWiederherstellen.push(a);
		redo.setEnabled(true);
	}

	/**
	 * Stellt die letzte, rueckgaengig gemachte Aktion wieder her.
	 */
	public void wiederherstellen() {
		if (!kannWiederherstellen())
			return;

		Aktion a = aktionenWiederherstellen.poll();
		a.wiederherstellen();
		redo.setEnabled(kannWiederherstellen());

		aktionenRueckgaengig.push(a);
		undo.setEnabled(true);
	}

}
