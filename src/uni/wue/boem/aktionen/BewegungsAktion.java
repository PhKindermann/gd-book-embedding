package uni.wue.boem.aktionen;

import uni.wue.boem.daten.Graph;
import uni.wue.boem.daten.Knoten;

public class BewegungsAktion implements Aktion {
	private Graph g;
	private Knoten knotenBewegt;
	private int startPosition, endPosition;

	public BewegungsAktion(Graph g, Knoten knotenInBewegung) {
		knotenBewegt = knotenInBewegung;
		this.g = g;
	}

	@Override
	public void rueckgaengig() {
		g.getKnoten().remove(knotenBewegt);
		g.getKnoten().add(startPosition, knotenBewegt);
	}

	@Override
	public void wiederherstellen() {
		g.getKnoten().remove(knotenBewegt);
		g.getKnoten().add(endPosition, knotenBewegt);
	}

	@Override
	public void starten() {
		startPosition = g.getKnoten().indexOf(knotenBewegt);
	}

	@Override
	public void beenden() {
		endPosition = g.getKnoten().indexOf(knotenBewegt);
	}

	@Override
	public boolean gibtEsUnterschiede() {
		return startPosition != endPosition;
	}

}
