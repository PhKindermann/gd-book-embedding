package uni.wue.boem.aktionen;

import uni.wue.boem.daten.Graph;
import uni.wue.boem.daten.Kante;
import uni.wue.boem.daten.Seite;

public class SeitenAktion implements Aktion {
	private Graph g;
	private Seite startSeite, endSeite;
	private Kante kante;

	public SeitenAktion(Graph g, Kante k) {
		this.kante = k;
		this.g = g;
	}

	@Override
	public void rueckgaengig() {
		kante.setSeite(startSeite);
	}

	@Override
	public void wiederherstellen() {
		kante.setSeite(endSeite);
	}

	@Override
	public void starten() {
		if (startSeite != null) {
			System.err.println("Action was started several times.");
			return;
		}

		for (Seite s : g.getSeiten()) {
			if (s.enthaeltKante(kante)) {
				startSeite = s;
				break;
			}
		}
	}

	@Override
	public void beenden() {
		if (startSeite == null) {
			System.err.println("Action wasn't started before ending.");
			return;
		}

		if (endSeite != null) {
			System.err.println("Action was ended several times.");
			return;
		}

		for (Seite s : g.getSeiten()) {
			if (s.enthaeltKante(kante)) {
				endSeite = s;
				break;
			}
		}
	}

	@Override
	public boolean gibtEsUnterschiede() {
		if (startSeite == null || endSeite == null)
			return false;

		return startSeite != endSeite;
	}

}
