package uni.wue.boem.aktionen;

public interface Aktion {

	public void rueckgaengig();

	public void wiederherstellen();

	public void starten();

	public void beenden();

	public boolean gibtEsUnterschiede();
}
