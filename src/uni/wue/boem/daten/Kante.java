package uni.wue.boem.daten;

import uni.wue.boem.elemente.Arc;

public class Kante {
	private Knoten[] knoten;
	private Seite seite;
	private Arc arc;

	public Kante(Knoten a, Knoten b, Seite s) {
		if (a == null || b == null)
			throw new IllegalArgumentException(
					"Vertex a and b must not be NULL.");

		this.knoten = new Knoten[2];
		this.knoten[0] = a;
		this.knoten[1] = b;

		for (int i = 0; i < knoten.length; i++) {
			knoten[i].getKanten().add(this);
		}

		this.seite = s;
		if (s != null)
			s.getKanten().add(this);

		this.arc = new Arc(a, b);
	}

	/**
	 * @return the seite
	 */
	public Seite getSeite() {
		return seite;
	}

	/**
	 * @param seite
	 *            the seite to set
	 */
	public void setSeite(Seite seite) {
		if (seite == null)
			throw new IllegalArgumentException(
					"The page assignment of an edge must not be NULL.");

		if (this.seite != null)
			this.seite.getKanten().remove(this);

		this.seite = seite;
		this.seite.getKanten().add(this);
	}

	/**
	 * @return the knoten
	 */
	public Knoten[] getKnoten() {
		return knoten;
	}

	public String toString() {
		return String.format("%s %s [%s]", (knoten[0].getNummer() < knoten[1]
				.getNummer()) ? knoten[0] : knoten[1],
				(knoten[0].getNummer() > knoten[1].getNummer()) ? knoten[0]
						: knoten[1], seite);
	}

	/**
	 * @return the arc
	 */
	public Arc getArc() {
		return arc;
	}
}
