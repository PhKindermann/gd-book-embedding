package uni.wue.boem.daten;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JColorChooser;

import uni.wue.boem.elemente.ColorIcon;

public class Seite {
	private int nummer;
	private Color farbe;
	private LinkedList<Kante> kanten;

	private LinkedList<JButton> farbButtons;
	private ActionListener farbWahlAktion;

	public Seite(int nummer, Color farbe) {
		if (nummer < 0)
			throw new IllegalArgumentException(
					"The label number of a page must be non-negative.");

		this.nummer = nummer;
		this.farbe = farbe;
		kanten = new LinkedList<Kante>();
	}

	/**
	 * @return the nummer
	 */
	public int getNummer() {
		return nummer;
	}

	/**
	 * @return the farbe
	 */
	public Color getFarbe() {
		return farbe;
	}

	/**
	 * @return the kanten
	 */
	public Kante[] getKantenArray() {
		return kanten.toArray(new Kante[kanten.size()]);
	}

	/**
	 * @return the kanten
	 */
	protected LinkedList<Kante> getKanten() {
		return kanten;
	}

	public String toString() {
		return nummer + "";
	}

	public boolean enthaeltKante(Kante k) {
		return kanten.contains(k);
	}

	public JButton getFarbButton() {
		if (farbButtons == null)
			farbButtons = new LinkedList<JButton>();

		JButton neuerFarbButton = new JButton(new ColorIcon(farbe));
		neuerFarbButton.addActionListener(getFarbWahlAktion());

		farbButtons.add(neuerFarbButton);

		return neuerFarbButton;
	}

	/**
	 * @return the farbWahlAktion
	 */
	private ActionListener getFarbWahlAktion() {
		if (farbWahlAktion == null)
			farbWahlAktion = new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					Color neu = JColorChooser.showDialog(null, "Choose color",
							getFarbe());

					if (neu != null) {
						farbe = neu;

						for (JButton jButton : farbButtons) {
							jButton.setIcon(new ColorIcon(farbe));
						}
					}
				}
			};
		return farbWahlAktion;
	}

}