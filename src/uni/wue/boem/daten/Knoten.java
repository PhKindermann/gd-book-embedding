package uni.wue.boem.daten;

import java.util.LinkedList;

import uni.wue.boem.elemente.Kreis;

public class Knoten implements Comparable<Knoten> {
	private int nummer;
	private LinkedList<Kante> kanten;
	private Kreis kreis;

	public Knoten(int nummer) {
		if (nummer < 0)
			throw new IllegalArgumentException(
					"The label number of a vertex must be non-negative.");

		this.nummer = nummer;
		this.kanten = new LinkedList<Kante>();
		this.kreis = new Kreis(this);
	}

	/**
	 * @return the nummer
	 */
	public int getNummer() {
		return nummer;
	}

	/**
	 * @return the kanten
	 */
	protected LinkedList<Kante> getKanten() {
		return kanten;
	}

	public String toString() {
		return nummer + "";
	}

	public boolean equals(Object o) {
		return o instanceof Knoten && ((Knoten) o).nummer == this.nummer;
	}

	/**
	 * @return the kreis
	 */
	public Kreis getKreis() {
		return kreis;
	}

	@Override
	public int compareTo(Knoten o) {
		if (o.getKreis().getMittelpunkt().x > this.getKreis().getMittelpunkt().x)
			return -1;
		else if (o.getKreis().getMittelpunkt().x < this.getKreis()
				.getMittelpunkt().x)
			return 1;
		return 0;
	}
}
