package uni.wue.boem.daten;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.util.LinkedList;
import java.util.Random;

import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.text.NumberFormatter;

public class Graph {
	private LinkedList<Knoten> knoten;
	private Seite[] seiten;
	private boolean editable;

	public Graph(LinkedList<Knoten> knoten, Seite[] seiten) {
		this.knoten = knoten;
		this.seiten = seiten;
		this.editable = false;
	}

	public void setzeKnotenAnPosition(Knoten k, int position) {
		knoten.remove(k);
		knoten.add(position, k);
	}

	/**
	 * @return the knoten
	 */
	public LinkedList<Knoten> getKnoten() {
		return knoten;
	}

	/**
	 * @return the seiten
	 */
	public Seite[] getSeiten() {
		return seiten;
	}

	public static Graph erzeugeNeuenGraphen() {
		JFormattedTextField anzahlKnoten = new JFormattedTextField(
				new NumberFormatter());
		JFormattedTextField anzahlSeiten = new JFormattedTextField(
				new NumberFormatter());

		JPanel labelPanel = new JPanel(new GridLayout(2, 1));
		labelPanel.add(new JLabel("Number of vertices: "));
		labelPanel.add(new JLabel("Number of pages: "));

		JPanel input = new JPanel(new GridLayout(2, 1));
		input.add(anzahlKnoten);
		input.add(anzahlSeiten);

		JPanel neuerGraph = new JPanel(new BorderLayout(5, 5));
		neuerGraph.add(labelPanel, BorderLayout.WEST);
		neuerGraph.add(input, BorderLayout.CENTER);

		String[] options = { "New graph", "Cancel" };

		int auswahl = 0;

		while (auswahl != 1) {
			auswahl = JOptionPane.showOptionDialog(null, neuerGraph,
					"Create new graph", JOptionPane.OK_CANCEL_OPTION,
					JOptionPane.PLAIN_MESSAGE, null, options, 1);

			if (auswahl == 0 && anzahlKnoten.getText().matches("\\d+")
					&& anzahlSeiten.getText().matches("\\d+"))
				break;
		}

		if (auswahl == 1)
			return null;

		LinkedList<Knoten> knoten = new LinkedList<Knoten>();
		for (int i = 0; i < Integer.parseInt(anzahlKnoten.getText()); i++) {
			knoten.add(new Knoten(i));
		}

		Random rnd = new Random();
		Seite[] seiten = new Seite[Integer.parseInt(anzahlSeiten.getText())];
		for (int i = 0; i < seiten.length; i++) {
			seiten[i] = new Seite(i, new Color(rnd.nextInt()));
		}

		Graph neu = new Graph(knoten, seiten);
		neu.editable = true;
		return neu;
	}

	/**
	 * @return the editable
	 */
	public boolean isEditable() {
		return editable;
	}

	public void kanteHinzufuegen() {

	}

	public void kanteEntfernen() {

	}
}
