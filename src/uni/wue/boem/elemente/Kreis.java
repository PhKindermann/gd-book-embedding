package uni.wue.boem.elemente;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;

import uni.wue.boem.daten.Knoten;

public class Kreis {
	private int radius;
	private Point mittelpunkt, startPunkt;
	private Knoten knoten;

	public Kreis(Knoten knoten) {
		mittelpunkt = new Point();
		startPunkt = new Point();

		this.knoten = knoten;
	}

	public void zeichne(Graphics2D g) {
		Color tmp = g.getColor();

		g.setColor(Color.WHITE);
		g.fillOval(startPunkt.x, startPunkt.y, radius, radius);

		g.setColor(Color.BLACK);
		g.drawOval(startPunkt.x, startPunkt.y, radius, radius);
		g.drawString(knoten.toString(), startPunkt.x, startPunkt.y);

		g.setColor(tmp);
	}

	public void bewege(int x, int y) {
		mittelpunkt.x = x;
		mittelpunkt.y = y;

		berechneStartpunkt();
	}

	private void berechneStartpunkt() {
		/*
		 * Der Radius muss halbiert abgezugen werden, da von einem Kreis die
		 * Linke untere Ecke als Startkoordinate geweahlt wird. Diese muss
		 * entsprechend vom Mittelpunkt verschoben werden.
		 */
		startPunkt.x = mittelpunkt.x - radius / 2;
		startPunkt.y = mittelpunkt.y - radius / 2;
	}

	public void berechneKoordinaten(int position, int hoehe, int breite,
			int anteilRadius, int anzahlKreise) {

		this.radius = berechneRadius(hoehe, breite, anteilRadius);

		mittelpunkt.x = berechneMittlePunktKoordinaten(position, breite,
				anzahlKreise);

		mittelpunkt.y = hoehe / 2;

		berechneStartpunkt();

	}

	public static int berechneRadius(int hoehe, int breite, int anteilRadius) {
		return Math.min(hoehe, breite) / anteilRadius;
	}

	/**
	 * Berechnet den Mittelpunkt der Knoten auf der x-Achse.
	 * 
	 * @param kreisposition
	 *            An welcher Position steht der Knoten.
	 * @return Mittelpunkt des Knotens
	 */
	public static int berechneMittlePunktKoordinaten(int kreisposition,
			int breite, int anzahlKreise) {
		int gleicheDistanz = breite / (anzahlKreise + 1);

		return gleicheDistanz * (kreisposition + 1);
	}

	public boolean isInside(int x, int y) {
		Point p = new Point(x, y);

		return p.distance(mittelpunkt) < radius / 2;
	}

	/**
	 * @return the mittelpunkt
	 */
	public Point getMittelpunkt() {
		return mittelpunkt;
	}
}
