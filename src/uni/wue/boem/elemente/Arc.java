package uni.wue.boem.elemente;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Stroke;
import java.util.Collections;
import java.util.LinkedList;

import uni.wue.boem.daten.Knoten;

public class Arc {
	private int breite, hoehe, startAngle, arcAngle;
	private LinkedList<Knoten> knoten;
	private boolean unten, oben, hervorheben;
	private Point startPunkt, mittelPunkt;

	public Arc(Knoten a, Knoten b) {
		knoten = new LinkedList<Knoten>();
		knoten.add(a);
		knoten.add(b);

		startAngle = 0;
		arcAngle = 180;

		startPunkt = new Point();
		mittelPunkt = new Point();
	}

	public void draw(Graphics2D g, Color farbe) {
		Color tmp = g.getColor();
		Stroke tmpStroke = g.getStroke();

		if (hervorheben)
			g.setStroke(new BasicStroke(((BasicStroke) tmpStroke)
					.getLineWidth() * 3));

		g.setColor(farbe);
		g.drawArc(startPunkt.x, startPunkt.y, breite, hoehe, startAngle,
				arcAngle);

		g.setStroke(tmpStroke);
		g.setColor(tmp);
	}

	/**
	 * Berechnet die Parameter zum Zeichnen des spezifischen Kreisbogens.
	 * 
	 * @param hoehe
	 *            Die H�he der Darstellung zur Bestimmung der Kr�mmung des
	 *            Bogens
	 */
	public void berechneKoordinaten(int hoehe) {
		if (hoehe <= 0) {
			System.err
					.println("Invalid height while computing of the circular arc. The height must be greater than 0.");
			return;
		}

		/*
		 * Sortiere die Knoten, damit die Ausrichtung immer positiv ist. Wichtig
		 * fuer den Fall, dass die Knoten durchs verschieben die Reihenfolge
		 * tauschen
		 */
		Collections.sort(knoten);

		this.startPunkt.x = knoten.getFirst().getKreis().getMittelpunkt().x;

		this.breite = knoten.getLast().getKreis().getMittelpunkt().x
				- this.startPunkt.x;
		this.hoehe = (int) Math.min(breite, hoehe * 0.9);

		this.startPunkt.y = knoten.getFirst().getKreis().getMittelpunkt().y
				- this.hoehe / 2;

		startAngle = 180;
		arcAngle = 180;

		if (oben) {
			startAngle = 0;
			if (unten)
				arcAngle = 360;
		}

		this.mittelPunkt.x = this.startPunkt.x + breite / 2;
		this.mittelPunkt.y = knoten.getFirst().getKreis().getMittelpunkt().y;
	}

	/**
	 * Berechnet den Mittelpunkt der Knoten auf der x-Achse.
	 * 
	 * @param kreisposition
	 *            An welcher Position steht der Knoten.
	 * @return Mittelpunkt des Knotens
	 */
	public static int berechneMittlePunktKoordinaten(int kreisposition,
			int breite, int anzahlKreise) {
		int gleicheDistanz = breite / (anzahlKreise + 1);

		return gleicheDistanz * (kreisposition + 1);
	}

	public String toString() {
		return String.format("(%d, %d): %d x %d, %d� -> %d�", startPunkt.x,
				startPunkt.y, breite, hoehe, startAngle, arcAngle);
	}

	/**
	 * @param unten
	 *            the unten to set
	 */
	public void setUnten(boolean unten) {
		this.unten = unten;
	}

	/**
	 * @param oben
	 *            the oben to set
	 */
	public void setOben(boolean oben) {
		this.oben = oben;
	}

	/**
	 * Die Koordinaten sind nahe der Bogens, wenn der Wert nahe 1 ist.
	 * 
	 * @param mouseX
	 *            x-Koordinate (des Mauszeigers z.B.)
	 * @param mouseY
	 *            y-Koordinate (des Mauszeigers z.B.)
	 * @return Ergebnis der Ellipsengleichung unter Ber�cksichtigung der
	 *         Ausrichtung. 100 bedeutet, die Koordinaten liegen nicht auf der
	 *         selben Seite, wie der Bogen.
	 */
	public double distanceToBorder(int mouseX, int mouseY) {
		if (!unten && !oben)
			return 0;

		double y = mittelPunkt.y - mouseY;
		boolean klickIstOben = y > 0;

		if ((unten && oben) || (klickIstOben && oben)
				|| (!klickIstOben && unten)) {

			double a = this.hoehe / 2;
			double b = this.breite / 2;

			double x = mouseX - mittelPunkt.x;

			return (((x * x) / (a * a)) + ((y * y) / (b * b)));
		}

		return 100;
	}

	/**
	 * @param hervorheben
	 *            the hervorheben to set
	 */
	public void setHervorheben(boolean hervorheben) {
		this.hervorheben = hervorheben;
	}

	/**
	 * @return the breite
	 */
	public int getBreite() {
		return breite;
	}
}
