package uni.wue.boem;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Collections;
import java.util.LinkedList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;

import uni.wue.boem.aktionen.AktionManager;
import uni.wue.boem.aktionen.BewegungsAktion;
import uni.wue.boem.aktionen.SeitenAktion;
import uni.wue.boem.daten.Graph;
import uni.wue.boem.daten.Kante;
import uni.wue.boem.daten.Knoten;
import uni.wue.boem.daten.Seite;
import uni.wue.boem.elemente.ColorIcon;

/**
 * Klasse f�r die Darstellung und Bearbeitung der Bucheinbettung.
 * 
 * @author Dennis van der Wals
 * @version 1.0.0
 *
 */
public class GraphGUI extends JPanel {
	private LinkedList<Seite> seitenOben, seitenUnten;
	private Graph graph;
	private AktionManager aktionManager;

	private int anteilRadius;
	private int mittlereKoordinate;
	private Knoten knotenInBewegung;
	private Kante nahsteKante;

	private ComponentListener compList;
	private MouseListener ml;
	private MouseMotionListener mml;
	private ActionListener al;

	/**
	 * Default Serial-ID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Standardkonstruktor. Erwartet einen AktionManager als Eingabe. Dar�ber
	 * werden die Knoepfe zum Rueckgaengig machen und wiederherstellen mit den
	 * entsprechenden Aktionen verknuepft.
	 * 
	 * @param aktionManager
	 *            AktionManager zum aufzeichnen und Anwenden von Aktionen, die
	 *            Rueckgaengig gemacht oder wiederhergestellt werden koennen.
	 */
	public GraphGUI(AktionManager aktionManager) {
		this.setLayout(new GridLayout());
		this.setBackground(Color.WHITE);

		anteilRadius = 25;
		this.aktionManager = aktionManager;

		this.addComponentListener(getCompList());
		this.addMouseListener(getMl());
		this.addMouseMotionListener(getMml());

		this.setDoubleBuffered(true);
	}

	@Override
	public void paint(Graphics g2) {
		super.paint(g2);

		Graphics2D g = (Graphics2D) g2;

		// Setzt eine hoehere Qualitaet der Graphik
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		g.setRenderingHint(RenderingHints.KEY_RENDERING,
				RenderingHints.VALUE_RENDER_QUALITY);

		// Zeichnet den "Buchruecken"
		g.setColor(Color.BLACK);
		g.drawLine(0, mittlereKoordinate, getWidth(), mittlereKoordinate);

		/*
		 * Prueft, ob ein Graph geladen ist um NullPointer Exceptions zu
		 * vermeiden
		 */
		if (graph != null) {
			// Zeichne Kanten
			for (Seite seite : getSeitenOben()) {
				for (Kante kante : seite.getKantenArray()) {
					kante.getArc().draw(g, seite.getFarbe());
				}
			}

			for (Seite seite : getSeitenUnten()) {
				for (Kante kante : seite.getKantenArray()) {
					kante.getArc().draw(g, seite.getFarbe());
				}
			}

			// Zeichne Knoten
			for (Knoten k : graph.getKnoten()) {
				k.getKreis().zeichne(g);
			}
		}

	}

	/**
	 * Eine Liste von Seiten, die auf der oberen haelfte angezeigt werden
	 * sollen.
	 * 
	 * @return the seitenOben
	 */
	public LinkedList<Seite> getSeitenOben() {
		if (seitenOben == null)
			seitenOben = new LinkedList<Seite>();
		return seitenOben;
	}

	/**
	 * Eine Liste von Seiten, die auf der unteren haelfte angezeigt werden
	 * sollen.
	 * 
	 * @return the seitenUnten
	 */
	public LinkedList<Seite> getSeitenUnten() {
		if (seitenUnten == null)
			seitenUnten = new LinkedList<Seite>();
		return seitenUnten;
	}

	/**
	 * Setzt den Graphen, der in der Buchbindung dargestellt werden soll.
	 * 
	 * @param graph
	 *            the graph to set
	 */
	public void setGraph(Graph graph) {
		this.graph = graph;
		seitenUnten = null;
		seitenOben = null;

		if (graph != null)
			berechneDarstellung();
	}

	/**
	 * Setzt fest, welcher Anteil an der gesamten Breite der Darstellung fuer
	 * die Kreise eingenommen werden darf.
	 * 
	 * @param anteilRadius
	 *            the anteilRadius to set
	 */
	public void setAnteilRadius(int anteilRadius) {
		this.anteilRadius = anteilRadius;

		if (graph != null)
			berechneDarstellung();
	}

	/**
	 * L�st alle Berechnungen aus, die f�r die korrekte Darstellung erforderlich
	 * sind. Dazu z�hlen die Berechnungen der Mittelpunkte der Knoten sowie die
	 * Kanten.
	 */
	public void berechneDarstellung() {
		berechneDarstellungKnoten(false);
		berechneDarstellungKanten(false);

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				repaint();
			}
		});
	}

	/**
	 * Fuert nur Neuberechnung und Zeichnung der Knoten aus. So koennen neue
	 * Knotenwerte berechnet werden, ohne die Kanten zu beeinflussen.
	 * 
	 * @param zeichnen
	 *            Legt fest, ob nach der Berechnung gezeichnet werden soll.
	 */
	private void berechneDarstellungKnoten(boolean zeichnen) {
		// Berechne die Kreise aller Knoten neu, die gezeichnet werden sollen.
		int index = 0;
		for (Knoten knoten : graph.getKnoten()) {
			knoten.getKreis().berechneKoordinaten(index++, getHeight(),
					getWidth(), anteilRadius, graph.getKnoten().size());
		}

		if (zeichnen)
			SwingUtilities.invokeLater(new Runnable() {

				@Override
				public void run() {
					repaint();
				}
			});
	}

	/**
	 * Fuert nur Neuberechnungen und Zeichnungen der Kanten aus.So koennen neue
	 * Kantenwerte berechnet werden, ohne die Knoten zu beeinflussen.
	 * 
	 * @param zeichnen
	 *            Legt fest, ob nach der Berechnung gezeichnet werden soll.
	 */
	private void berechneDarstellungKanten(boolean zeichnen) {
		// Setze alle Positionen der Boegen zurueck.
		for (Seite seite : graph.getSeiten()) {
			for (Kante kante : seite.getKantenArray()) {
				kante.getArc().setOben(false);
				kante.getArc().setUnten(false);
			}
		}

		// Setze Position der oberen Boegen
		for (Seite seite : getSeitenOben()) {
			for (Kante kante : seite.getKantenArray()) {
				kante.getArc().setOben(true);
				kante.getArc().berechneKoordinaten(getHeight());
			}
		}

		// Setzte Position der unteren Boegen
		for (Seite seite : getSeitenUnten()) {
			for (Kante kante : seite.getKantenArray()) {
				kante.getArc().setUnten(true);
				kante.getArc().berechneKoordinaten(getHeight());
			}
		}

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				repaint();
			}
		});
	}

	/**
	 * Ersetzt den AktionManager.
	 * 
	 * @param aktionManager
	 *            the aktionManager to set
	 */
	public void setAktionManager(AktionManager aktionManager) {
		this.aktionManager = aktionManager;
	}

	/**
	 * Erzeugt den ComponentListener und gibt ihn zurueck. Loest eine
	 * Neuzeichnen und berechnen aus, wenn die Groesse des Fensters sich
	 * aendert.
	 * 
	 * @return the compList ComponentenListener, der auf eine
	 *         Groessenveraenderung des Fensters reagiert.
	 */
	private ComponentListener getCompList() {
		if (compList == null)
			compList = new ComponentListener() {

				@Override
				public void componentHidden(ComponentEvent e) {
				}

				@Override
				public void componentMoved(ComponentEvent e) {
				}

				@Override
				public void componentResized(ComponentEvent e) {
					mittlereKoordinate = getHeight() / 2;

					if (graph != null)
						berechneDarstellung();
				}

				@Override
				public void componentShown(ComponentEvent e) {
				}
			};
		return compList;
	}

	/**
	 * Erzeugt einen MouseListener und gibt ihn zurueck. Dieser regrisitriert
	 * Mausaktionen des Benutzers und steuert entsprechendes Verhalten.
	 * 
	 * @return the ml MouseListener, der Benutzereingaben verarbeitet.
	 */
	private MouseListener getMl() {
		if (ml == null)
			ml = new MouseListener() {

				@Override
				public void mouseReleased(MouseEvent e) {
					legeKnotenAb();
				}

				@Override
				public void mousePressed(MouseEvent e) {
					nehmeKnotenAuf(e.getX(), e.getY());
				}

				@Override
				public void mouseExited(MouseEvent e) {
				}

				@Override
				public void mouseEntered(MouseEvent e) {
				}

				@Override
				public void mouseClicked(MouseEvent e) {
					verschiebeNahsteKante(e.getX(), e.getY());
				}
			};
		return ml;
	}

	/**
	 * Erzeugt einen MouseMotionListener und gibt ihn zurueck. Wird verwendet um
	 * Knoten zu bewegen und Kanten hervorzuheben.
	 * 
	 * @return the mml MouseMotionListener
	 */
	private MouseMotionListener getMml() {
		if (mml == null)
			mml = new MouseMotionListener() {

				@Override
				public void mouseMoved(MouseEvent e) {
					bestimmeNahsteKante(e.getX(), e.getY());
				}

				@Override
				public void mouseDragged(MouseEvent e) {
					bewegeKnoten(e.getX());
				}
			};
		return mml;
	}

	/**
	 * Pr�ft, ob die Koordinaten in auf einem Knoten liegen, und initialisiert
	 * dessen Bewegung. Dazu wird eine Bewegungsaktion gestartet, sodass die
	 * Bewegung rueckgaengig gemacht werden kann und der Knoten wird als
	 * beweglicher Knoten markiert. Er folgt den Eingaben der Funktion
	 * bewegeKnoten().
	 * 
	 * @param x
	 *            x-Koordinate (des Mauszeigers z.B.)
	 * @param y
	 *            y-Koordinate (des Mauszeigers z.B.)
	 */
	private void nehmeKnotenAuf(int x, int y) {
		if (graph == null)
			return;

		// Pruefe, ob ein Knoten angeklickt wurde
		for (Knoten knoten : graph.getKnoten()) {
			if (knoten.getKreis().isInside(x, y)) {
				knotenInBewegung = knoten;

				if (aktionManager != null)
					aktionManager.starteNeueAktion(new BewegungsAktion(graph,
							knotenInBewegung));

				break;
			}
		}
	}

	/**
	 * Bewegt einen Knoten zur angegebenen x-koordinate entlang der mittleren
	 * Achse. Dabei wird gepr�ft, ob der die Reihenfolge der Darstellung der
	 * Reihenfolge der Knoten entspricht.
	 * 
	 * @param x
	 *            x-Koordinate (des Mauszeigers z.B.)
	 */
	private void bewegeKnoten(int x) {
		if (knotenInBewegung == null)
			return;

		knotenInBewegung.getKreis().bewege(x, mittlereKoordinate);

		Collections.sort(graph.getKnoten());
		berechneDarstellungKnoten(true);

		knotenInBewegung.getKreis().bewege(x, mittlereKoordinate);
		berechneDarstellungKanten(true);

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				repaint();
			}
		});
	}

	/**
	 * Legt einen Knoten, der in Bewegung ist, an der letzten Position zu der er
	 * bewegt wurde mit bewegeKnoten(int x, int y) ab.
	 */
	private void legeKnotenAb() {
		if (knotenInBewegung == null)
			return;

		knotenInBewegung = null;

		if (aktionManager != null)
			aktionManager.beendeAktiveAktion();

		Collections.sort(graph.getKnoten());
		berechneDarstellung();

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				repaint();
			}
		});
	}

	/**
	 * Bestimmt, ob die Koordinaten nahe einer Kante sind und hebt die nahste
	 * Kante hervor.
	 * 
	 * @param x
	 *            x-Koordinate (des Mauszeigers z.B.)
	 * @param y
	 *            y-Koordinate (des Mauszeigers z.B.)
	 */
	private void bestimmeNahsteKante(int x, int y) {
		if (graph == null)
			return;

		/*
		 * Prueft, welche Kante die Ellipsengleichung mit den Koordinaten des
		 * Mauszeigers am besten erf�llt. Dabei ist ein Schwellwert f�r den
		 * maximalen Abstand vorgegeben.
		 */
		double distance = Double.MAX_VALUE;
		Kante nahsteKante = null;
		for (Seite seite : getSeitenOben()) {
			for (Kante kante : seite.getKantenArray()) {
				double d = Math.abs(kante.getArc().distanceToBorder(x, y) - 1);
				if (d < 0.2 && d < distance) {
					distance = d;
					nahsteKante = kante;
				}
			}
		}
		for (Seite seite : getSeitenUnten()) {
			for (Kante kante : seite.getKantenArray()) {
				double d = Math.abs(kante.getArc().distanceToBorder(x, y) - 1);
				if (d < 0.2 && d < distance) {
					distance = d;
					nahsteKante = kante;
				}
			}
		}

		/*
		 * Wenn eine andere Kante zuvor hervorgehoben wurde, wird diese nun
		 * nicht mehr hervorgehoben.
		 */
		if (this.nahsteKante != null)
			this.nahsteKante.getArc().setHervorheben(false);

		this.nahsteKante = nahsteKante;

		/*
		 * Auf Grund des maximalen Abstandes, kann es sein, dass es keine nahste
		 * Kante gibt.
		 */
		if (nahsteKante != null)
			// Die nahste Kante wird hervorgehoben
			this.nahsteKante.getArc().setHervorheben(true);

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				repaint();
			}
		});
	}

	/**
	 * Verschiebt die nahste Kante zur Auswahl auf eine andere Seite. Sind nur
	 * zwei Seiten vorhanden, wird die jeweils andere Seite ausgewaehlt,
	 * ansonsten ein Popup menue aufgerufen.
	 * 
	 * @param x
	 *            x-Koordinate an der das Popup Menue erscheint
	 * @param y
	 *            y-Koordinate an der das Popup Menue erscheint
	 */
	private void verschiebeNahsteKante(int x, int y) {
		if (nahsteKante == null)
			return;

		aktionManager.starteNeueAktion(new SeitenAktion(graph, nahsteKante));

		if (graph.getSeiten().length > 2)
			erzeugeMenu(nahsteKante).show(this, x, y);

		else if (graph.getSeiten()[0].enthaeltKante(nahsteKante))
			verschiebeKante(1);
		else
			verschiebeKante(0);
	}

	/**
	 * Erzeugt ein Popup Menue mit allen Eintraegen, die fuer die nahste Kante
	 * relevant sind. Es werden alle Seiten ausgewaehlt, bis auf die, auf der
	 * die Kante gerade liegt. Bei Auswahl einer Seite, wird die Kante auf diese
	 * Seite verschoben.
	 * 
	 * @param k
	 *            Kante, fuer die das Menue erzeugt wird.
	 * @return Gibt das Menue zurueck, in dem ausgewaehlet werden kann, auf
	 *         welche Seite die Kante verschoben werden soll.
	 */
	private JPopupMenu erzeugeMenu(Kante k) {
		JPopupMenu menu = new JPopupMenu("Move edge to::");

		menu.add(new JLabel("move to page: "));
		menu.addSeparator();

		// Fuege jede Seite hinzu
		for (Seite seite : graph.getSeiten()) {
			if (!seite.enthaeltKante(k)) {
				JMenuItem item = new JMenuItem(seite.toString(), new ColorIcon(
						seite.getFarbe()));
				item.addActionListener(getAl());
				item.setActionCommand(seite.toString());

				menu.add(item);
			}
		}

		return menu;
	}

	/**
	 * Erzeugt einen ActionListener und gibt ihn zurueck. Dieser verwaltet die
	 * Aktion beim benutzen des Popup Menues und loest die Verschiebung einer
	 * Kante von einer Seite auf eine andere aus.
	 * 
	 * @return the al ActionListener
	 */
	private ActionListener getAl() {
		if (al == null) {
			al = new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					verschiebeKante(Integer.parseInt(e.getActionCommand()));
				}
			};
		}
		return al;
	}

	/**
	 * Verschiebt eine Kante auf eine Zielseite. Die Zielseite kann durch eine
	 * Zahl angegeben werden, da sie sortiert in einem Array vorliegen und damit
	 * immer eindeutig zuordbar sind. Aktuallisiert anschliessend die
	 * Darstellung.
	 * 
	 * @param zielSeite
	 *            Index der Zielseite im Array = Nummer der Zielseite
	 */
	private void verschiebeKante(int zielSeite) {
		if (nahsteKante == null)
			return;

		nahsteKante.setSeite(graph.getSeiten()[zielSeite]);
		aktionManager.beendeAktiveAktion();

		berechneDarstellung();
	}
}
