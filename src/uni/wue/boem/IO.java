package uni.wue.boem;

import java.awt.Color;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Random;
import java.util.Scanner;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import uni.wue.boem.daten.Graph;
import uni.wue.boem.daten.Kante;
import uni.wue.boem.daten.Knoten;
import uni.wue.boem.daten.Seite;

/**
 * Klasse zum laden und speichern einer Bucheinbettung,
 * 
 * @author Dennis van der Wals
 * @version 1.0.0
 *
 */
public class IO {
	private static Seite[] seiten;
	private static LinkedList<Knoten> knoten;

	/**
	 * Methode zum speichern des uebergebenen Graphen. Fragt den Benutzer, wo
	 * gespeichert werden soll.
	 * 
	 * @param g
	 *            Graph der gespeichert werden soll.
	 * @return 0 = nicht gespeichert, 1 = gespeichert, -1 = fehler
	 */
	public static int speichern(Graph g) {
		if (g == null)
			return 0;

		return speichern(g.getKnoten(), g.getSeiten());
	}

	/**
	 * Die Funktion speichert die �bergebenen Arrays im richtigem Format in eine
	 * ausgewaehlte Datei.
	 * 
	 * @param k
	 *            LinkedList mit Knoten.
	 * @param s
	 *            Array mit Seiten.
	 * @return 0 = nicht gespeichert, 1 = gespeichert, -1 = fehler
	 */
	public static int speichern(LinkedList<Knoten> k, Seite[] s) {
		knoten = k;
		seiten = s;

		JFileChooser jf = new JFileChooser();

		int auswahl = jf.showSaveDialog(null);

		/*
		 * Wird nur aktiv, wenn der Benutzer die Auswahl einer Datei best�tigt
		 * hat.
		 */
		if (auswahl == JFileChooser.CANCEL_OPTION)
			return 0;

		if (jf.getSelectedFile().isDirectory())
			System.err.println("A directory cannot be chosen!");

		else if (jf.getSelectedFile().exists())
			auswahl = JOptionPane
					.showConfirmDialog(
							null,
							"The chosen file already exists. Shall it be overwritten?",
							"Overwrite file", JOptionPane.YES_NO_OPTION,
							JOptionPane.QUESTION_MESSAGE);

		else
			auswahl = JOptionPane
					.showConfirmDialog(
							null,
							"The chosen file doesn't exists yet. Shall it be created?",
							"Create file", JOptionPane.YES_NO_OPTION,
							JOptionPane.QUESTION_MESSAGE);

		/*
		 * Wenn eine g�ltige Datei ausgew�hlt und die notwendige Operation
		 * (erzeugen/ueberschreiben) bestaetigt wurde.
		 */
		if (auswahl == JOptionPane.YES_OPTION) {
			try {
				if (!jf.getSelectedFile().exists()
						&& !jf.getSelectedFile().createNewFile()) {
					System.err.println("File could not be created.");
					return -1;
				}
			} catch (IOException e) {
				e.printStackTrace();
				return -1;
			}

			// Schreibt die Daten in die neue Datei,
			try {
				schreibe(jf.getSelectedFile());
				System.out.println("File was saved.");
				return 1;

			} catch (IOException e) {
				System.err.println("File could not be saved.");
				e.printStackTrace();
				return -1;
			}
		}

		return 0;
	}

	/**
	 * Oeffnet eine ausgewaehlte Datei und uebertraegt die Knoten, Kanten und
	 * Seiten in die statischen Arrays: knoten, seiten.
	 */
	public static boolean laden() {
		JFileChooser jf = new JFileChooser();

		int auswahl = jf.showOpenDialog(null);

		if (auswahl != JFileChooser.CANCEL_OPTION
				&& jf.getSelectedFile().isFile()) {
			try {
				lese(jf.getSelectedFile());
				System.out.println("File was loaded.");
				return true;
			} catch (FileNotFoundException e) {
				System.err.println("File could not be loaded.");
				e.printStackTrace();
				return false;
			}
		}
		return false;
	}

	/**
	 * Schreibt die Daten aus den Arrays knoten und seiten im bestimmten Format
	 * in eine Datei.
	 * 
	 * @param f
	 *            Zieldatei
	 * @throws IOException
	 *             Wirft ausnahmen bei Dateizugriffsfehlern.
	 */
	private static void schreibe(File f) throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter(f));

		bw.write("# Number of Nodes (N)");
		bw.newLine();
		bw.write(knoten.size() + "");
		bw.newLine();

		bw.write("# Number of pages (K)");
		bw.newLine();
		bw.write(seiten.length + "");
		bw.newLine();

		bw.write("# Permutation of nodes");
		bw.newLine();
		for (Knoten k : knoten) {
			bw.write(k.toString());
			bw.newLine();
		}

		bw.write("# Edges with starting node, ending node and assigned page in rectangular brackets");
		bw.newLine();
		for (int i = 0; i < seiten.length; i++) {
			for (int j = 0; j < seiten[i].getKantenArray().length; j++) {
				bw.write(seiten[i].getKantenArray()[j].toString());
				bw.newLine();
			}
		}

		bw.close();
	}

	/**
	 * Liest Knoten, deren Reihenfolge, Seiten und Kanten aus einer Datei aus.
	 * 
	 * @param f
	 *            Datei mit Knoten, Permutation, Seiten und Kanten
	 * @throws FileNotFoundException
	 */
	private static void lese(File f) throws FileNotFoundException {
		Scanner dateiScanner = new Scanner(f);
		String zeile = null;

		knoten = null;
		seiten = null;

		// Finde Anzahl der Knoten
		int anzahlKnoten = 0;
		while (dateiScanner.hasNextLine()) {
			zeile = entferneKommentar(dateiScanner.nextLine());

			if (zeile.matches("\\d+")) {
				knoten = new LinkedList<Knoten>();
				anzahlKnoten = Integer.parseInt(zeile);
				break;
			}
		}

		if (anzahlKnoten == 0) {
			dateiScanner.close();
			throw new IllegalArgumentException(
					"Number of vertices could not be found..");
		}

		// Finde Anzahl der Seiten
		Random rnd = new Random();
		while (dateiScanner.hasNextLine()) {
			zeile = entferneKommentar(dateiScanner.nextLine());

			if (zeile.matches("\\d+")) {
				seiten = new Seite[Integer.parseInt(zeile)];

				for (int i = 0; i < seiten.length; i++) {
					Color c = Color.BLACK;

					/*
					 * Erzeugt zuf�llige Farben, wenn mehr als 2 Seiten ben�tigt
					 * werden.
					 */
					if (seiten.length > 2)
						c = new Color(rnd.nextInt());

					seiten[i] = new Seite(i, c);
				}

				break;
			}
		}

		if (seiten == null) {
			dateiScanner.close();
			throw new IllegalArgumentException(
					"Number of pages could not be found.");
		}

		// Lese die Reihenfolge der Knoten
		while (knoten.size() < anzahlKnoten) {
			zeile = entferneKommentar(dateiScanner.nextLine());

			if (zeile.matches("\\d+")) {
				Knoten neuerKnoten = new Knoten(Integer.parseInt(zeile));

				if (knoten.contains(neuerKnoten)) {
					dateiScanner.close();
					throw new IllegalArgumentException("The vertex "
							+ neuerKnoten
							+ " exists twice in the permutation.");
				}

				knoten.add(neuerKnoten);
			}

			if (!dateiScanner.hasNextLine()) {
				dateiScanner.close();
				throw new IllegalArgumentException(
						"The permutation of the vertices is incomplete.\n Only "
								+ knoten.size()
								+ " vertices were found in the permutation.");
			}
		}

		// Lese Kanten ein
		int kanten = 0;
		while (dateiScanner.hasNextLine()) {
			zeile = entferneKommentar(dateiScanner.nextLine());

			if (zeile.matches("\\d+ \\d+ \\[\\d+\\]")) {
				/*
				 * Zerteile die Zeile um die einzelnen Zahlen interpretieren zu
				 * k�nnen.
				 */
				String[] pars = zeile.split(" ");
				pars[2] = pars[2].replace("[", "").replace("]", "");

				int a = Integer.parseInt(pars[0]);
				int b = Integer.parseInt(pars[1]);
				int page = Integer.parseInt(pars[2]);

				Knoten ka = null;
				Knoten kb = null;

				/*
				 * Suche die Knoten anhand der hinterlegten Nummer. Dies ist
				 * wichtig, da die Permutation nicht immer mit der Nummerierung
				 * �bereinstimmen muss.
				 */
				for (int i = 0; i < knoten.size() && (ka == null || kb == null); i++) {
					Knoten vergleich = knoten.get(i);
					if (vergleich.getNummer() == a) {
						ka = vergleich;

					} else if (vergleich.getNummer() == b) {
						kb = vergleich;
					}
				}

				/*
				 * Erzeugt eine neue Kante, die sp�ter �ber die entsprechende
				 * Seite oder Knoten abgerufen werden kann. Es ist nich
				 * notwendig die Kanten in einer zus�tzlichen Datenstruktur
				 * abzulegen.
				 */
				new Kante(ka, kb, seiten[page]);
				kanten++;
			}
		}

		dateiScanner.close();

		if (kanten == 0)
			throw new IllegalArgumentException(
					"No edges were found.");
	}

	/**
	 * Entfernt alle Symbole aus einem String die nach einem Kommentarsymbol #
	 * stehen, inklusive des Symbols.
	 * 
	 * @param eingabe
	 *            String, der von Kommentaren befreit werden soll
	 * @return String ohne Kommentar
	 */
	private static String entferneKommentar(String eingabe) {
		int kommentarStart = eingabe.indexOf("#");

		if (kommentarStart != -1)
			return eingabe.substring(0, kommentarStart).trim();

		return eingabe;
	}

	/**
	 * @return the seiten
	 */
	public static Seite[] getSeiten() {
		return seiten;
	}

	/**
	 * @return the knoten
	 */
	public static LinkedList<Knoten> getKnoten() {
		return knoten;
	}
}
