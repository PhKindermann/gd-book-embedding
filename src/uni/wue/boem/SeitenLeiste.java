package uni.wue.boem;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.LinkedList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.BevelBorder;

import uni.wue.boem.daten.Graph;
import uni.wue.boem.daten.Seite;

public class SeitenLeiste extends JPanel {
	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = 2866932457576245063L;

	private HashMap<JCheckBox, Seite> seiteProCheckbox;
	private LinkedList<Seite> aktiveSeiten;
	private ActionListener actionL;

	private GraphGUI gui;

	public SeitenLeiste(GraphGUI gui, LinkedList<Seite> aktiveSeiten) {
		this.gui = gui;
		this.aktiveSeiten = aktiveSeiten;
	}

	public void setGraph(Graph g, LinkedList<Seite> aktiveSeiten) {
		if (g == null)
			return;

		this.aktiveSeiten = aktiveSeiten;

		JPanel panel = new JPanel(new GridLayout(1, g.getSeiten().length + 1,
				5, 5));

		seiteProCheckbox = new HashMap<JCheckBox, Seite>();

		panel.add(new JLabel("active pages:"));
		for (Seite seite : g.getSeiten()) {
			panel.add(erzeugePanelProSeite(seite));
		}

		this.removeAll();
		this.setLayout(new BorderLayout());
		this.add(panel, BorderLayout.WEST);

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				revalidate();
				repaint();
			}
		});
	}

	private JPanel erzeugePanelProSeite(Seite s) {
		JPanel p = new JPanel(new BorderLayout(5, 5));
		JButton button = s.getFarbButton();
		button.addActionListener(getActionL());

		JCheckBox box = new JCheckBox(s.getNummer() + "");
		box.setSelected(false);
		box.addActionListener(getActionL());

		p.add(box, BorderLayout.WEST);
		p.add(button, BorderLayout.CENTER);
		p.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));

		seiteProCheckbox.put(box, s);

		return p;
	}

	/**
	 * @return the actionL
	 */
	private ActionListener getActionL() {
		if (actionL == null) {
			actionL = new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					if (e.getSource() instanceof JCheckBox)
						if (((JCheckBox) e.getSource()).isSelected())
							aktiveSeiten
									.add(seiteProCheckbox.get(e.getSource()));

						else
							aktiveSeiten.remove(seiteProCheckbox.get(e
									.getSource()));

					SwingUtilities.invokeLater(new Runnable() {

						@Override
						public void run() {
							repaint();
						}
					});

					gui.berechneDarstellung();
				}
			};
		}
		return actionL;
	}
}
