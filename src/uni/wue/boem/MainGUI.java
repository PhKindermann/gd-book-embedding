package uni.wue.boem;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import uni.wue.boem.aktionen.AktionManager;
import uni.wue.boem.daten.Graph;

/**
 * Hauptklasse fuer die Buchbindung. Stellt Startmethode und Konstrukt der
 * graphischen Oberflaeche sowie Bedienelemente bereit.
 * 
 * @author Dennis van der Wals
 * @version 1.0.0
 *
 */
public class MainGUI {
	/* GUI Elemente */
	private JButton speichern, oeffnen, rueckgaengig, wiederherstellen,
			kanteHinzufuegen, kanteEntfernen, neuerGraph;
	private ActionListener buttonActions;
	private GraphGUI graphGUI;
	private SeitenLeiste oben, unten;
	private JSlider anteilRadius;
	private JFrame frame;
	private WindowListener windowClosing;

	/* Daten Elemente */
	private Graph graph;
	private AktionManager aktionManager;

	/**
	 * Startmethode
	 * 
	 * @param args
	 *            keine Funktion
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {

		}
		new MainGUI();
	}

	/**
	 * Initialisiert die Oberflaeche und die Aktionen.
	 */
	public MainGUI() {
		buildGUI();
		aktionenManagerZuruecksetzen();

		getFrame().setVisible(true);
	}

	/**
	 * Hier wird die Oberflaeche zusammengesetzt und Designed.
	 */
	private void buildGUI() {
		JPanel buttons = new JPanel(new GridLayout(1, 4, 5, 5));
		buttons.add(getNeuerGraph());
		buttons.add(getOeffnen());
		buttons.add(getSpeichern());
		buttons.add(getRueckgaengig());
		buttons.add(getWiederherstellen());
		buttons.add(getKanteHinzufuegen());
		buttons.add(getKanteEntfernen());

		JPanel top = new JPanel(new BorderLayout(5, 5));
		top.add(buttons, BorderLayout.WEST);
		top.add(getAnteilRadius(), BorderLayout.EAST);

		JPanel buch = new JPanel(new BorderLayout(5, 5));
		buch.add(getGraphGUI(), BorderLayout.CENTER);
		buch.add(new JScrollPane(getOben(),
				JScrollPane.VERTICAL_SCROLLBAR_NEVER,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS), BorderLayout.NORTH);
		buch.add(new JScrollPane(getUnten(),
				JScrollPane.VERTICAL_SCROLLBAR_NEVER,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS), BorderLayout.SOUTH);

		getFrame().setLayout(new BorderLayout(5, 5));
		getFrame().add(top, BorderLayout.NORTH);
		getFrame().add(buch, BorderLayout.CENTER);
	}

	/**
	 * @return the speichern
	 */
	private JButton getSpeichern() {
		if (speichern == null) {
			speichern = new JButton(new ImageIcon(
					MainGUI.class.getResource("images/Save16.gif")));
			speichern.setEnabled(false);
			speichern.setToolTipText("Save");
			speichern.addActionListener(getButtonActions());
		}
		return speichern;
	}

	/**
	 * @return the oeffnen
	 */
	private JButton getOeffnen() {
		if (oeffnen == null) {
			oeffnen = new JButton(new ImageIcon(
					MainGUI.class.getResource("images/Open16.gif")));
			oeffnen.setEnabled(true);
			oeffnen.setToolTipText("Open");
			oeffnen.addActionListener(getButtonActions());
		}
		return oeffnen;
	}

	/**
	 * @return the rueckgaengig
	 */
	private JButton getRueckgaengig() {
		if (rueckgaengig == null) {
			rueckgaengig = new JButton(new ImageIcon(
					MainGUI.class.getResource("images/Undo16.gif")));
			rueckgaengig.setEnabled(false);
			rueckgaengig.setToolTipText("Undo");
			rueckgaengig.addActionListener(getButtonActions());
		}
		return rueckgaengig;
	}

	/**
	 * @return the wiederherstellen
	 */
	private JButton getWiederherstellen() {
		if (wiederherstellen == null) {
			wiederherstellen = new JButton(new ImageIcon(
					MainGUI.class.getResource("images/Redo16.gif")));
			wiederherstellen.setEnabled(false);
			wiederherstellen.setToolTipText("Redo");
			wiederherstellen.addActionListener(getButtonActions());
		}
		return wiederherstellen;
	}

	/**
	 * @return the buttonActions
	 */
	private ActionListener getButtonActions() {
		if (buttonActions == null) {
			buttonActions = new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					if (e.getSource() == getSpeichern()) {
						if (IO.speichern(graph) == 1) {

							/*
							 * Nachdem eine Datei gespeichert wurde, kann keine
							 * Aktion rueckgaengig oder wiederholt werden.
							 */

							aktionenManagerZuruecksetzen();
						}

					} else if (e.getSource() == getOeffnen()) {

						if (graphWechselVorbereiten()) {
							if (IO.laden())
								setGraph(new Graph(IO.getKnoten(),
										IO.getSeiten()));
							else
								return;
						}

					} else if (e.getSource() == getRueckgaengig()) {
						aktionManager.rueckgaengig();
						getGraphGUI().berechneDarstellung();

					} else if (e.getSource() == getWiederherstellen()) {
						aktionManager.wiederherstellen();
						getGraphGUI().berechneDarstellung();

					} else if (e.getSource() == getNeuerGraph()) {
						if (graphWechselVorbereiten()) {
							Graph neu = Graph.erzeugeNeuenGraphen();
							if (neu != null)
								setGraph(neu);
							else
								return;
						}

					} else if (e.getSource() == getKanteHinzufuegen()) {
						graph.kanteHinzufuegen();

					} else if (e.getSource() == getKanteEntfernen()) {
						graph.kanteEntfernen();

					}

					SwingUtilities.invokeLater(new Runnable() {

						@Override
						public void run() {
							getFrame().revalidate();
							getFrame().repaint();
						}
					});
				}
			};
		}
		return buttonActions;
	}

	private boolean graphWechselVorbereiten() {
		/*
		 * Prueft, ob ungespeicherte Aenderungen vorliegen und fragt, ob diese
		 * erst gespeichert werden sollen.
		 */
		// int auswahl = pruefeAufAenderungen();

		switch (pruefeAufAenderungen()) {
		case JOptionPane.CANCEL_OPTION:
			// Bricht den Graphwechsel ab.
			return false;

		case JOptionPane.NO_OPTION:
			// Ersetzt den Graphen ohne Ruecksicht auf bisherige Graphen.
			return true;

		default:
			break;
		}

		/* Versucht die Aenderungen zu speichern */
		boolean error = IO.speichern(graph) == -1;

		/*
		 * Bei einem Fehler beim Speichern wird der Benutzer gefragt, ob die
		 * neue Datei dennoch geladen werden soll.
		 */
		int auswahl = JOptionPane.NO_OPTION;
		if (error)
			auswahl = JOptionPane
					.showConfirmDialog(
							null,
							"Changes could not be saved.\n Load new file anyway?",
							"Don't save changes.",
							JOptionPane.YES_NO_OPTION,
							JOptionPane.ERROR_MESSAGE);

		/*
		 * Liegt kein Fehler vor oder ist der Benutzer bereit, die Daten zu
		 * verlieren, wird die neue Datei geoeffnet.
		 */
		return (auswahl == JOptionPane.YES_OPTION || !error);
	}

	/**
	 * Prueft anhand des Rueggaengig-Knopfes, ob Aenderungen durchgefuehrt
	 * wurden, die noch nicht gespeichert sind. Oeffnet dann einen Dialog um zu
	 * fragen, ob diese Aenderungen gespeichert werden sollen. Gibt es keine
	 * Aenderungen oder sollen diese verworfen werden, wird
	 * JOptionPane.NO_OPTION zurueck gegeben.
	 * 
	 * @return JOptionPane.NO_OPTION wenn Aenderungen verworfen werden sollen,
	 *         JOptionPane.CANCEL_OPTION, wenn abgebrochen werden soll und
	 *         JOptionPane.YES_OPTION, wenn die Aenderungen gespeichert werden
	 *         sollen.
	 */
	private int pruefeAufAenderungen() {
		int auswahl = JOptionPane.NO_OPTION;
		if (getRueckgaengig().isEnabled()) {
			auswahl = JOptionPane.showConfirmDialog(null,
					"Shall the changes be saved?",
					"Save changes.", JOptionPane.YES_NO_CANCEL_OPTION,
					JOptionPane.QUESTION_MESSAGE);

		}
		return auswahl;
	}

	/**
	 * @return the graphGUI
	 */
	public GraphGUI getGraphGUI() {
		if (graphGUI == null)
			graphGUI = new GraphGUI(aktionManager);
		return graphGUI;
	}

	/**
	 * @param graph
	 *            the graph to set
	 */
	public void setGraph(Graph graph) {
		this.graph = graph;

		/*
		 * Setze das Minimum des Radius so, dass immer alle Knoten angezeigt
		 * werden koennen. Andernfalls koennte der Anteil an der Darstellung
		 * groesser werden, als die Darstellung selbst ist
		 */
		getAnteilRadius().setMinimum(graph.getKnoten().size());
		getAnteilRadius().setMaximum(getAnteilRadius().getMinimum() * 40);

		getSpeichern().setEnabled(true);
		aktionenManagerZuruecksetzen();

		getGraphGUI().setGraph(graph);
		getOben().setGraph(graph, getGraphGUI().getSeitenOben());
		getUnten().setGraph(graph, getGraphGUI().getSeitenUnten());

		erlaubeBearbeitung(graph.isEditable());

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				getFrame().revalidate();
				getFrame().repaint();
			}
		});
	}

	/**
	 * @return the anteilRadius
	 */
	public JSlider getAnteilRadius() {
		if (anteilRadius == null) {
			anteilRadius = new JSlider(JSlider.HORIZONTAL, 5, 200, 25);
			anteilRadius
					.setToolTipText("Determines the size of the vertices as fraction of the shown area.");
			anteilRadius.addChangeListener(new ChangeListener() {

				@Override
				public void stateChanged(ChangeEvent e) {
					getGraphGUI().setAnteilRadius(anteilRadius.getValue());
				}
			});
		}
		return anteilRadius;
	}

	/**
	 * Ersetzt den alten AktionenManager gegen einen neuen, da dessen Aktionen
	 * nicht mehr gueltig sind.
	 */
	private void aktionenManagerZuruecksetzen() {
		this.aktionManager = new AktionManager(getRueckgaengig(),
				getWiederherstellen());
		getGraphGUI().setAktionManager(this.aktionManager);
	}

	/**
	 * @return the oben
	 */
	private SeitenLeiste getOben() {
		if (oben == null)
			oben = new SeitenLeiste(getGraphGUI(), getGraphGUI()
					.getSeitenOben());
		return oben;
	}

	/**
	 * @return the unten
	 */
	private SeitenLeiste getUnten() {
		if (unten == null)
			unten = new SeitenLeiste(getGraphGUI(), getGraphGUI()
					.getSeitenUnten());
		return unten;
	}

	/**
	 * @return the frame
	 */
	private JFrame getFrame() {
		if (frame == null) {
			frame = new JFrame("GD-Book-Embedding");
			frame.setSize(800, 600);
			frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
			frame.addWindowListener(getWindowClosing());
		}
		return frame;
	}

	/**
	 * @return the windowClosing
	 */
	private WindowListener getWindowClosing() {
		if (windowClosing == null)
			windowClosing = new WindowListener() {

				@Override
				public void windowOpened(WindowEvent e) {
				}

				@Override
				public void windowIconified(WindowEvent e) {
				}

				@Override
				public void windowDeiconified(WindowEvent e) {
				}

				@Override
				public void windowDeactivated(WindowEvent e) {
				}

				@Override
				public void windowClosing(WindowEvent e) {
					switch (pruefeAufAenderungen()) {
					case JOptionPane.YES_OPTION:
						IO.speichern(graph);

					case JOptionPane.NO_OPTION:
						System.exit(0);
						break;

					default:
						break;
					}

				}

				@Override
				public void windowClosed(WindowEvent e) {
				}

				@Override
				public void windowActivated(WindowEvent e) {
				}
			};

		return windowClosing;
	}

	/**
	 * @return the kanteHinzufuegen
	 */
	private JButton getKanteHinzufuegen() {
		if (kanteHinzufuegen == null) {
			kanteHinzufuegen = new JButton(new ImageIcon(
					MainGUI.class.getResource("images/Add16.gif")));
			kanteHinzufuegen.setEnabled(false);
			kanteHinzufuegen.setToolTipText("Add edge");
			kanteHinzufuegen.addActionListener(getButtonActions());
			kanteHinzufuegen.setVisible(false);
		}
		return kanteHinzufuegen;
	}

	/**
	 * @return the kanteEntfernen
	 */
	private JButton getKanteEntfernen() {
		if (kanteEntfernen == null) {
			kanteEntfernen = new JButton(new ImageIcon(
					MainGUI.class.getResource("images/Remove16.gif")));
			kanteEntfernen.setEnabled(false);
			kanteEntfernen.setToolTipText("Remove edge");
			kanteEntfernen.addActionListener(getButtonActions());
			kanteEntfernen.setVisible(false);
		}
		return kanteEntfernen;
	}

	/**
	 * @return the neuerGraph
	 */
	private JButton getNeuerGraph() {
		if (neuerGraph == null) {
			neuerGraph = new JButton(new ImageIcon(
					MainGUI.class.getResource("images/New16.gif")));
			neuerGraph.setToolTipText("Create new graph");
			neuerGraph.addActionListener(getButtonActions());
		}
		return neuerGraph;
	}

	private void erlaubeBearbeitung(boolean erlaube) {

		getKanteEntfernen().setEnabled(erlaube);
		getKanteEntfernen().setVisible(erlaube);

		getKanteHinzufuegen().setEnabled(erlaube);
		getKanteHinzufuegen().setVisible(erlaube);
	}

}
